<?php
include 'public_header.php';
  $uid=$_SESSION['usr_id'];
if(isset($_GET['id'])){
  $id=$_GET['id'];
  unset($_SESSION['pid'][$id]);
  unset($_SESSION['name'][$id]);
  unset($_SESSION['qty'][$id]);
  unset($_SESSION['price'][$id]);
}
if(isset($_POST['pid'])){

$pid= $_POST['pid'];
$qty=$_POST['qty'];
$name= $_POST['name'];
$price=$_POST['price'];
$catid = $_POST['cat_id'];
if($qty==""){
	$qty = 1;
}
$arr = array();

if(isset($_SESSION['pid'])){
	
	$arr = $_SESSION['pid'];
	array_push($arr, $pid);
}else{
	array_push($arr, $pid);
}
$_SESSION['pid'] = $arr;
$qarr = array();

if(isset($_SESSION['qty'])){
	
	$qarr = $_SESSION['qty'];
	array_push($qarr, $qty);
}else{
	array_push($qarr, $qty);
}
$_SESSION['qty'] = $qarr;

$narr = array();

if(isset($_SESSION['name'])){
	
	$narr = $_SESSION['name'];
	array_push($narr, $name);
}else{
	array_push($narr, $name);
}
$_SESSION['name'] = $narr;

$parr = array();

if(isset($_SESSION['price'])){
	
	$parr = $_SESSION['price'];
	array_push($parr, $price);
}else{
	array_push($parr, $price);
}
$_SESSION['price'] = $parr;
  header ("location: public_menu.php?page_id=$catid");
}



?>

<div class="container col-md-offset-2 col-md-8">
  <h2>Your Cart</h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Pid</th>
        <th>Name</th>
        <th>Qty</th>
        <th>Unit Price</th>
        <th>Total</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
        
        $j=count($_SESSION['pid']);
        $gtotal=0;
        for($i=0;$i<$j;$i++){
          if(isset($_SESSION['pid'][$i])){
           

          echo '<tr>';
          echo '<td>'.$_SESSION['pid'][$i].'</td>';
          echo '<td>'.$_SESSION['name'][$i].'</td>';
          echo '<td>'.$_SESSION['qty'][$i].'</td>';
          echo '<td>'.$_SESSION['price'][$i].'</td>';
          $var = $_SESSION['qty'][$i]*$_SESSION['price'][$i];
          $gtotal =$gtotal+$var;
          echo '<td>'.$var.'</td>';
          echo '<td><a href = "cart.php?id='.$i.'"> <span class="glyphicon glyphicon-remove"></span></a></td>';
        }else{
          $j++;
        }
        }
          $product= implode(",", $_SESSION['pid']);
          $quantity= implode(",", $_SESSION['qty']);
        echo $product;
        echo $quantity;


      ?>
        <tr>
          <td></td>
          <td><b>Grand Total</b></td>
          <td></td>
          <td></td>
          <td><b><?php echo $gtotal; ?></b></td>
          <td></td>
      

    </tbody>
  </table>
  <form id="form1" name="form1" method="post" action="chkout.php">
            <input type="hidden" name="product" id="pid" value="<?php echo $product?>" />
            <input type="hidden" name="quantity" id="pid" value="<?php echo $quantity?>" />
            <input type="hidden" name="uid" id="pid" value="<?php echo $uid?>" />
            <input type="hidden" name="gtotal" id="pid" value="<?php echo $gtotal?>" />
            
                      
           <div class="radio-inline ">
              <input type="radio" id="to" name="type" value="take out">Take out
            </div>
            <div class="radio-inline">
              <input type="radio" id="del" name="type" value="delivery"> Delivery
            </div>
              <button  class="btn btn-success" type="submit" name="buttoncheckout" id="button" >Check Out</button>
  </form>
</div>


</body>
</html>

<!-- Written By Bishal Adhikari 069/BCT/612-->