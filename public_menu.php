<?php
include('public_header.php');
//confirm_logged_in();

?>

<!-- Begin Menu page Body-->
<div class="panel panel-default col-md-offset-2 col-md-8 ">
    <div class="panel-body>">
        <h3 style="font-weight:bold;font-style:Italic">Menu</h3>

        <nav class="navbar navbar-default orderBy col-md-3 " role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".orderBy-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <span class="visible-xs navbar-brand " style="color:#fff;">Ordering Options</span>
            </div>

            <div class="navbar-collapse collapse orderBy-collapse ">
                <h5 style="font-weight: bold;font-style:italic;">ORDER BY</h5>

                <div class="nav"><a href="public_menu.php?page_id=category"><h5
                                style="font-weight: bold;font-style:italic;">CATEGORY</h5></a>
                    <ul style="list-style:none;padding:5px;">
                        <?php
                        if ($page_id = 'category') {
                            $category_list = "";
                            $query_run = mysqli_query($con, "SELECT * FROM categories");
                            $product_count = mysqli_num_rows($query_run);

                            if ($product_count > 0) {
                                while ($row = mysqli_fetch_array($query_run)) {

                                    $cat_id = $row['id'];
                                    $name = $row['cat_name'];
                                    $category_list .= '<li';
                                    if ($cat_id == $_GET['page_id']) {
                                        $category_list .= ' style="font-weight:bold;"';
                                    }
                                    $category_list .= '><a href="public_menu.php?page_id=' . $cat_id . '">' . $name . '</a></li>';
                                }
                                echo $category_list;
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="pull-left"><a href="public_menu.php?page_id=list2"><h5
                                style="font-weight: bold;font-style:italic;">Featured</h5></a>
                    <?php

                    echo
                    '<ul style="list-style:none;padding:5px;">

				<li> recommended item</li>
				<li> first category</li>
				<li> first category</li>
				<li> first category</li>
				<li> first category</li>


				</ul>';

                    ?>

                </div>
                <div class="pull-left"><a href="public_menu.php?page_id=list2"><h5
                                style="font-weight: bold;font-style:italic;">Specials</h5></a>
                    <?php

                    echo
                    '<ul style="list-style:none;padding:5px;">
				<li> recommended item</li>
				<li> first category</li>
				<li> first category</li>
				<li> first category</li>
				<li> first category</li>


				</ul>';

                    ?>

                </div>
                <div class="pull-left"><a href="public_menu.php?page_id=list2"><h5
                                style="font-weight: bold;font-style:italic;">Best Items</h5></a>
                    <?php

                    echo
                    '<ul style="list-style:none;padding:5px;">

				<li> recommended item</li>
				<li> first category</li>
				<li> first category</li>
				<li> first category</li>
				<li> first category</li>


				</ul>';

                    ?>

                </div>
            </div>
        </nav>
        <div class=" col-md-9">

            <div class="row"><?php
                if ($_GET['page_id'] != 'category') {


                    $cat_id = $_GET['page_id'];

                    $item_list = "";
                    $item_query_run = mysqli_query($con, "SELECT * FROM items where `cat_id`=$cat_id");
                    $item_count = mysqli_num_rows($item_query_run);
                    if ($item_count > 0) {
                        while ($item_row = mysqli_fetch_array($item_query_run)) {

                            $item_id = $item_row['id'];
                            $item_name = $item_row['item_name'];
                            $item_desc = $item_row['item_desc'];
                            $item_image = $item_id . '.jpg';
                            $item_price = $item_row['price'];


                            $item_list .= ' <div class="col-sm-6 col-md-4" style="padding:1px;">
					    <div class="thumbnail" >
					    <div >
							<img class="img-responsive" src="assets/images/' . $item_image . '" alt="item image" style="height:150px;">
							</div>
						      
						      <div class="caption" >
						        <h5 style="font-weight:bold;">' . $item_name . '</h5>
						        <p style="  font-weight:bold; font-size:15px; font-style:italic;"> NPR.' . $item_price . '</p>
						        
						        
								<form id="form1" name="form1" method="post" action="cart.php" class="form-group">
									<input type="text" class="form-control pull-left" name="qty" placeholder="QTY" style="margin-bottom: 5px">
							        <input type="hidden" name="pid" id="pid" value="' . $item_id . '" />
							        <input type="hidden" name="name" id="name" value="' . $item_name . '" />
							        <input type="hidden" name="price" id="price" value="' . $item_price . '" />
							        <input type="hidden" name="cat_id" id="cat_id" value="' . $_GET['page_id'] . '" />
							        <div >
							        <a href="product_details.php?name=' . $item_name . '&ratings=" class="btn btn-success" role="button">Details</a>
							        <button  class="btn btn-danger pull-right" type="submit" name="button" id="button" >Add to Cart</button>
							        </div>
							     </form>
						      </div>
						    </div>
					  </div>';

                        }

                        echo $item_list;
                    } else {
                        $item_list = '<div class="alert-danger" style="padding:5px;">No Items to display</div>';
                        echo $item_list;

                    }
                }
                //include 'product_panel.php';?>
            </div>

        </div>
    </div>
</div>
<!-- Written By Bishal Adhikari 069/BCT/612-->