<?php
include('modal.php');
include '../connect.inc.php';




?>


<!DOCTYPE html>

<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scaleable=no">


    <title>Admin Page</title>
 <!--background: 
  #79A9BF;-->
    
    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/stylesheet/css/primary.css" rel="stylesheet">
    <link href="../assets/stylesheet/css/animate.css" rel="stylesheet">
    <script src="../assets/bootstrap/jquery/jquery.min.js"></script>
    <script src="../assets/bootstrap/jquery/jquery.js"></script>

    <!--<script src="../assets/bootstrap/js/bootstrap.min.js"></script>-->
   <script src="../assets/bootstrap/js/bootstrap.js"></script>

  </head>
  <body style="background:#E1EEF5">


    <!--  Top Navigation bar-->
        <nav class="navbar navbar-default myNavbar blue-color" role="navigation">
            <div class="container-fluid col-md-8">
                <!-- Header First line-->
                <div class="row table" >
                    <ul  class=" admin-nav-ul "  > 
                   <!-- <span class="glyphicon glyphicon-search " aria-hidden="true"></span> -->
                        <li ><a href="order.php" ><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span><br>Orders</a></li>

                        <li ><a href="restaurant_info.php" ><span class="glyphicon glyphicon-comment" aria-hidden="true"></span><br>Restaurant info</a></li>
                        


                        <li><a href="menuCategories.php?pageid=category" ><span class="glyphicon glyphicon-cutlery" aria-hidden="true"></span><br>Menu</a></li>
                        <li><a href="#" ><span class="glyphicon glyphicon-user" aria-hidden="true"></span><br>Clients</a></li>
                        <li><a href="#" ><span class="glyphicon glyphicon-user" aria-hidden="true"></span><br>Users</a></li>
                        <li><a href="#" ><span class="glyphicon glyphicon-gift" aria-hidden="true"></span><br>Discounts</a></li>


                        
                        

                    </ul>
                 
                </div>
            </div>
<!-- right side navigation-->
            <div class="container-fluid col-md-2 ">
                
                <div class="row ">
                    <ul  class=" admin-nav-right-ul"> 
                        <li> <a href="#"data-toggle="modal" data-target="#adminSettingsModal">Account</li>
                        
                        <li><a href="#" >Logout</a></li>

                    </ul>
                 
                </div>
            </div>

        </nav>
        <br>


 <!-- Written By Bishal Adhikari 069/BCT/612-->