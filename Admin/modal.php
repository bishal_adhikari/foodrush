

            <!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
  <div class="modal-dialog" role="document">
   <div class="panel panel-default">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php if ($_GET['pageid']=='category'){echo "Add New Category"; }
        if ($_GET['pageid']=='product'){echo "Add New Product"; }
        if ($_GET['pageid']=='extras'){echo "Add New Extras"; }


        ?></h4>
      </div>
      <div class="modal-body">

          <!--Form goes here-->
            <form action="menuCategories.php?pageid=category" class="form-horizontal" role="form" method="POST">
              <div class="form-group">
                <label class="control-label col-sm-4" for="lable_cat_name">Category Name:</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="cat_name" placeholder="Enter name">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4" for="lable_cat_desc" >Descriptions:</label>
                <div class="col-sm-8"> 
                    <textarea class="form-control" rows ="4" cols="20" name="cat_desc" placeholder="Enter descriptions"></textarea>
                </div>
              </div>
              <div class="form-group "> 
                <div class="col-sm-offset-4 col-xs-8 ">
                  <button type="submit" class="btn btn-primary pull-right" >Save</button>
                </div>
              </div>
            </form>
      </div>

    </div>
</div>
  </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myproductmodal">
  <div class="modal-dialog" role="document">
   <div class="panel panel-default">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php if ($_GET['pageid']=='category'){echo "Add New Category"; }
        if ($_GET['pageid']=='product'){echo "Add New Product"; }
        if ($_GET['pageid']=='extras'){echo "Add New Extras"; }


        ?></h4>
      </div>
      <div class="modal-body">

          <!--Form goes here-->
            <form action="menuCategories.php?pageid=category" class="form-horizontal" role="form" method="POST">
              <div class="form-group">
                <label class="control-label col-sm-4" for="lable_cat_name">Category Name:</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="cat_name" placeholder="Enter name">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4" for="lable_cat_desc" >Descriptions:</label>
                <div class="col-sm-8"> 
                    <textarea class="form-control" rows ="4" cols="20" name="cat_desc" placeholder="Enter descriptions"></textarea>
                </div>
              </div>
              <div class="form-group "> 
                <div class="col-sm-offset-4 col-xs-8 ">
                  <button type="submit" class="btn btn-primary pull-right" >Save</button>
                </div>
              </div>
            </form>
      </div>

    </div>
</div>
  </div>
</div>
<!-- modal for category exists alerts and messages-->
                    <div class="modal fade" id="categoryExistsModal" tabindex="-1" role="dialog" aria-labelledby="msgmodal">
                        <div class="modal-dialog" role="document">
                         <div class="panel panel-default">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                            the category already exists.
                            </div>

                          </div>
                        </div>
                        </div>
                      </div>

<!-- modal for successfull added alerts and messages-->
                    <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="msgmodal">
                        <div class="modal-dialog" role="document">
                         <div class="panel panel-default">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                            Successfully added
                            </div>

                          </div>
                        </div>
                        </div>
                      </div>

              <!-- modal for Admin Account settings-->
                    <div class="modal fade" id="adminSettingsModal" tabindex="-1" role="dialog" aria-labelledby="adminSettingsModal">
                        <div class="modal-dialog" role="document">
                         <div class="panel panel-default">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                            Change passwords...
                            </div>

                          </div>
                        </div>
                        </div>
                      </div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myproductmodal">
  <div class="modal-dialog" role="document">
   <div class="panel panel-default">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php if ($_GET['pageid']=='category'){echo "Add New Category"; }
        if ($_GET['pageid']=='product'){echo "Add New Product"; }
        if ($_GET['pageid']=='extras'){echo "Add New Extras"; }


        ?></h4>
      </div>
      <div class="modal-body">

           <!--Form goes here-->
           
            <form action="items.php?cat_id=<?php echo $_GET['cat_id'];?>" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">

              <div class="form-group">
                <label class="control-label col-sm-4" for="lbl_item_name">Item Name:</label>
                <div class="col-sm-8"> 
                  <input type="text" class="form-control" name="item_name" placeholder="Item Name">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4" for="lable_item_desc">Item Descriptions:</label>
                <div class="col-sm-8"> 
              <textarea class="form-control" rows ="4" cols="20"  name ="item_desc" placeholder="Enter descriptions"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4" for="country">Item Ingredients:</label>
                <div class="col-sm-8"> 
                  <input type="text" class="form-control" name="item_ing" placeholder="Item ingredients">
                </div>
              </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="label_price">Price:</label>
                  <div class="col-sm-2"> 
                  <input type="text" class="form-control" name="price_type" placeholder="NPR">
                </div>
                <div class="col-sm-2"> 
                  <input type="text" class="form-control" name="price_amt" placeholder="30">
                </div>
              </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="image_label">Image:</label>
                <div class="col-sm-2"> 
                   <div class="thumbnail pull-left" style="margin:2px;">
                     <img src="../assets/images/43.jpg" width="150px" height="150px" alt="item image" >
                   </div>
                  <input type="file" name="image" id="image">

                </div>

            </div>


              <div class="form-group "> 
                <div class="col-sm-offset-4 col-sm-8">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>

            </form>
      </div>

    </div>
</div>
  </div>
</div>