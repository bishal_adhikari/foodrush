<!DOCTYPE html>

<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scaleable=no">


    <title>Admin Page</title>

    
    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/stylesheet/css/primary.css" rel="stylesheet">
    <link href="../assets/stylesheet/css/animate.css" rel="stylesheet">
    <script src="../assets/bootstrap/jquery/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.js"></script>
  </head>

  <body>

    <!--  Top Navigation bar-->
            <nav class="navbar navbar-default" style = "background: #555;">
              <div class="container-fluid">
                <div class="navbar-header" >
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="glyphicon glyphicon-triangle-bottom"></span>

                  </button>
                  <a class="navbar-brand" href="#" style = "color: #ddd;">FoodRush</a>
                </div>
                <div class="collapse navbar-collapse" id = "myNavbar">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" style = "color: #ddd;">Account settings</a></li>
                    <li><a href="../pages/log_out.php" style = "color: #ddd;">Logout</a></li>
                  </ul>
                </div>
              </div>
            </nav>

    <!--  Top Navigation bar-->
    <div class="container">
<div class="row">
  <div class="col-md-3">
    <div class="sidebar-nav">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="visible-xs navbar-brand">Navigations</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
          <ul class="nav navbar-nav" >
            <h5 style="margin:0;padding:10px;background:#8D0D19;color:#ddd;">BUSINESS INFORMATIONS </h5>
            <li><a href="index.php?id=details">Restaurant Details</a></a></li>
            <li><a href="menu.php?id=menu">Menu</a>

            </li>
            <li><a href="#">Online Orders</a></li>
            <li><a href="#">Reservations</a></li>
            <li><a href="#">Events</a></li>
            <li><a href="#">Users Account</a></li>
           
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>